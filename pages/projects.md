---
layout: page-fullwidth
title: "Mis trabajos"
subheadline: "Portfolio"
teaser: "Estos son algunos de los trabajos que he realizado por el momento"

---
<!--more-->

<div class="row t60">
    <div class="medium-6 columns b30">
        <img src="{{ site.urlimg }}pilote2.png" alt="">
        <p><span style="font-weight:bold">Pilote<sup>2</sup> Ecosystem</span></p>
        <p>Sistema empotrado para equipos de gestión energética pertenecientes a <a href="http://effiautomation.com">effiautomation</a></p>
        <p>Tecnologías utilizadas: Laravel, jQuery, HTML5</p>
    </div><!-- /.medium-6.columns -->

    <div class="medium-6 columns b30">
        <img src="{{ site.urlimg }}podmanager.png" alt="">
        <p><span style="font-weight:bold">Podcast manager</span></p>
        <p>Aplicación para gestionar la radio online de <a href="http://radiopodcastellano.es"> radio podcastellano</a></p>
        <p>Tecnologías utilizadas: Django, jQuery</p>
    </div><!-- /.medium-6.columns -->
</div><!-- /.row -->


<div class="row t30">
    <div class="medium-6 columns">
        <img src="{{ site.urlimg }}tiojuarez.png" alt="">
        <p><a href="http://tiojuarez.es">Tío Juárez</a></p>
        <p>Restaurante de comida rápida tex-mex. Trabajo encargado por <a href="http://www.keiva.es">KEIVA</a></p>
        <p>Tecnologías utilizadas: Wordpress</p>
    </div><!-- /.medium-4.columns -->

    <div class="medium-6 columns">
        <img src="{{ site.urlimg }}bvazafatas.png" alt="">
        <p><a href="http://bvazafatas.es">B&V azafatas</a></p>
        <p>Trabajo encargado por <a href="http://www.keiva.es">KEIVA</a></p>
        <p>Tecnologías utilizadas: Wordpress</p>
    </div><!-- /.medium-4.columns -->
</div><!-- /.row -->

<div class="row t30">
    <div class="medium-6 columns">
        <img src="{{ site.urlimg }}hoppstore.png" alt="">
        <p><a href="http://hoppstore.com">Hoppstore</a></p>
        <p>Tienda de ropa para niños</p>
        <p>Tecnologías utilizadas: Prestashop</p>
    </div><!-- /.medium-4.columns -->

    <div class="medium-6 columns">
        <img src="{{ site.urlimg }}i5ingenieria.png" alt="">
        <p><a href="http://i5ingenieria.com">i5 ingeniería</a></p>
        <p>Trabajo encargado por <a href="http://www.keiva.es">KEIVA</a></p>
        <p>Tecnologías utilizadas: Wordpress</p>
    </div><!-- /.medium-4.columns -->
</div><!-- /.row -->
