---
layout: page-fullwidth
title: "Proyectos personales"
subheadline: "Proyectos"
teaser: "Trabajos personales que tienen que ver con programación."

---
<!--more-->

<div class="row t60">
    <div class="medium-6 columns b30">
        <img src="{{ site.urlimg }}nursecurse.gif" alt="">
        <p><span style="font-weight:bold">Nurse curse</span></p>
        <p>
        Mi primer videojuego para móviles android. Voy actualizando el código en <a href="https://github.com/rubendelblanco/nursecurse">GitHub</a>.
        Este juego es un clon del antiguo Solar Fox de Atari 2600.
        </p>
        <p>Tecnologías utilizadas: Java, libgdx</p>
    </div><!-- /.medium-6.columns -->

    <div class="medium-6 columns b30">
        <img src="{{ site.urlimg }}rolemaster.jpeg" alt="">
        <p><span style="font-weight:bold">Rolemaster Suite</span></p>
        <p>Como fanático de <a href="http://ironcrown.com/rolemaster/">Rolemaster</a> siempre he querido un sistema para gestionar este complejo juego de rol. Esto es un intento para conseguirlo</p>
        <p>Tecnologías utilizadas: Laravel, jQuery</p>
    </div><!-- /.medium-6.columns -->
</div><!-- /.row -->
