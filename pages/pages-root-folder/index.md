---
#
# Use the widgets beneath and the content will be
# inserted automagically in the webpage. To make
# this work, you have to use › layout: frontpage
#
layout: page-fullwidth

#
# Use the call for action to show a button on the frontpage
#
# To make internal links, just use a permalink like this
# url: /getting-started/
#
# To style the button in different colors, use no value
# to use the main color or success, alert or secondary.
# To change colors see sass/_01_settings_colors.scss
#
callforaction:
  url: https://tinyletter.com/feeling-responsive
  text: Inform me about new updates and features ›
  style: alert
permalink: /index.html
#
# This is a nasty hack to make the navigation highlight
# this page as active in the topbar navigation
#
homepage: true
---
<div class="row t30">
  <div class="medium-4 columns frontpage-widget">
    <a href="{{site.url}}/blog" style="border-bottom:0px">
    <img src="{{site.urlimg}}blog.png" alt="" width="128">
    </a>
    <h2 class="font-size-h3 t10">Blog</h2>
    <p>Mis anotaciones sobre programación, seguridad y todo lo que se me ocurra sobre informática.</p>
  </div>
  <div class="medium-4 columns frontpage-widget">
    <a href="{{site.url}}/pages/portfolio" style="border-bottom:0px">
    <img src="{{ site.urlimg }}portfolio.png" alt="" width="128">
    </a>
    <h2 class="font-size-h3 t10">Portfolio</h2>
    <p>¿Por lo que sea quieres contratarme? Si quieres ver algunos de mis trabajos, aquí los tienes y así te haces una idea de lo que puedo hacer.</p>
  </div>
  <div class="medium-4 columns frontpage-widget">
    <a href="{{site.url}}/pages/proyectos" style="border-bottom:0px">
    <img src="{{ site.urlimg }}project.png" alt="" width="128">
    </a>
    <h2 class="font-size-h3 t10">Proyectos</h2>
    <p>Como buen fanático del código, estos son otros trabajos que estoy haciendo por mi cuenta en mis horas libres.</p>
  </div>
</div>
<div class="row t30">
  <h1>Mis skills</h1>
  <div class="medium-6 columns frontpage-widget">
    <h4>PHP</h4>
    <progress value="75" max="100"></progress>
  </div>
  <div class="medium-6 columns frontpage-widget">
    <h4>Laravel</h4>
    <progress value="70" max="100"></progress>
  </div>
  <div class="medium-6 columns frontpage-widget">
    <h4>Python</h4>
    <progress value="60" max="100"></progress>
  </div>
  <div class="medium-6 columns frontpage-widget">
    <h4>Django</h4>
    <progress value="50" max="100"></progress>
  </div>
  <div class="medium-6 columns frontpage-widget">
    <h4>Java</h4>
    <progress value="70" max="100"></progress>
  </div>
  <div class="medium-6 columns frontpage-widget">
    <h4>Javascript (jQuery et al.)</h4>
    <progress value="50" max="100"></progress>
  </div>
  <div class="medium-12 columns frontpage-widget">
  <h4>Otros</h4>
  Github, Wordpress, Prestashop, CSS3, Bootstrap, Android.
  </div>
</div>
