---
layout: page
subheadline: Proyectos
title:  "Rolemaster suite ¡gestión de combate completada!"
teaser: "El primer hito de Rolemaster suite, resolver los combates, está hecho."
meta_teaser: "El sistema de gestión de combates de Rolemaster está hecho. Y ha sido un trabajo largo."
breadcrumb: false
categories:
    - proyectos
    - desarrollo web
    - rolemaster suite
tags:
    - laravel
author: rdelblanco
comments: true
---
Una pequeña entrada para decir que la gestión de los combates de Rolemaster ya casi está terminada. Creí que esta travesía por el desierto sería eterna, pero no. Pronto habrá un vídeo enseñando las particularidades de la aplicación. Mientras tanto, si quieres ver cómo va la cosa, puedes ver el desarrollo en mi cuenta de <a href="https://www.livecoding.tv/rubendelblanco/">livecoding.tv</a>.
