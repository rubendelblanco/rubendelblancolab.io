---
layout: page
subheadline: Proyectos
title:  "Rolemaster Suite: progreso del proyecto en imágenes"
teaser: "Rolemaster Suite: progreso del proyecto en imágenes"
meta_teaser: "Rolemaster Suite: progreso del proyecto en imágenes"
breadcrumb: false
categories:
    - proyectos
    - desarrollo web
    - rolemaster suite
tags:
    - laravel
author: rdelblanco
comments: true
---

Como una imagen vale más que mil palabras, voy a poner algunas capturas de distintos menús de Rolemaster Suite a fecha de hoy. Por supuesto, a medida que el trabajo avance, esto puede cambiar.

<figure class="text-center"><a href="{{ site.urlimg }}chat.jpg"><img class="img-responsive img-rounded center-block" src="{{ site.urlimg }}chat.jpg" alt="mensjaes rolemaster" /></a> <figcaption> <small>
 A veces viene bien poder mandarse mensajes cuando no interesa que los demás jugadores sepan qué se está tramando.
</small>

<figure class="text-center"><a href="{{ site.urlimg }}lista_personajes.jpg"><img class="img-responsive img-rounded center-block" src="{{ site.urlimg }}lista_personajes.jpg" alt="listado personajes" /></a> <figcaption> <small>
Base de datos de personajes, tanto jugadores como no jugadores.
</small>

<figure class="text-center"><a href="{{ site.urlimg }}combate.jpg"><img class="img-responsive img-rounded center-block" src="{{ site.urlimg }}combate.jpg" alt="combate rolemaster" /></a> <figcaption> <small>
Gracias a la base de datos de personajes, se pueden añadir rápidamente en el combate tantos personajes como necesitemos.
</small>
