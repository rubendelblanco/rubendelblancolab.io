---
layout: page
subheadline: Proyectos
title:  "Rolemaster suite cancelado... de momento"
teaser: "Explico por qué he dejado el desarrollo de esta aplicación"
meta_teaser: "Una manera de comunicarse con los datos de AEMET"
breadcrumb: false
categories:
    - proyectos
    - desarrollo web
    - rolemaster suite
tags:
    - laravel
author: rdelblanco
---
<p>
Como ya había dicho en <a href="https://rubendelblanco.gitlab.io/proyectos/desarrollo%20web/rolemaster%20suite/rolemaster-suite-gestion-de-combates-casi-completada/">otro post</a>, había terminado la gestión de combates, el primer hito del largo camino que supone hacer un gestor de Rolemaster completo. En mi mente tenía proyectado hacer los siguientes pasos:
<ul>
  <li><b>Sistema de combates.</b> Ya terminado, tenía preferencia porque es lo que más tiempo lleva en una partida.</li>
  <li><b>Conteo de experiencia.</b> Terminado también. Esto elimina lo farragoso de tener que mirar tablas de puntos de experiencia cada vez que se hace una maniobra o se inflige una herida.</li>
  <li><b>Creación y subida de nivel de personajes.</b> En este punto fue donde dejé el proyecto. Luego explico por qué.</li>
  <li><b>Creación de objetos mágicos y cálculo de su valor.</b> Otra cosa especialmente farragosa si hay que hacerla "a mano".</li>
  <li><b>Sistema que permite comprar a los jugadores desde su dispositivo móvil.</b> Como si de un juego RPG se tratara, permitiría a los jugadores comprar todos a la vez. Usando el método clásico, el Máster o Director de juego solo puede atender uno a uno a los jugadores cuando quieren comprar/vender cosas. Un ordenador permite atender a todos a la vez.</li>
  <li><b>Otros</b> Como, por ejemplo, habilitar un mapa de la zona con localizaciones y posibles misiones que están disponibles al estilo de los juegos modernos como Fallout o la saga Elder Scrolls.</li>
</ul>
</p>
<p>
Bien, pues aunque el proyecto es atractivo y disfrutaba mucho haciéndolo, resulta que nunca encuentro un grupo para jugar y probarlo. Y es un trabajo duro y largo como para no usarlo. ¿Para qué echar horas y horas en una aplicación que apenas se va a usar? Rolemaster no es un juego de rol mayoritario (en un hobby, el rol de lápiz y papel, que ya es minoritario de por si) por lo que además sé que no voy a contar con mucho feedback por la red. Esa es una razón.
</p>
<p>
La otra razón es que he comprobado que la aplicación necesita de mucho código frontend, o sea, javascript (al fin y al cabo es como crear el engine de un videojuego). Por eso me he dado cuenta de que lo mejor es rehacer el código a una tecnología tipo Node o Angular. Además, esta fue la primera aplicación que hice con Laravel y el código es bastante desastroso. Resumiendo: que si algún día retomo este proyecto sería con otro framework más enfocado a javascript.
</p>
<p>
Es por esto que hoy por hoy Rolemaster Suite queda en la nevera. Veremos si resucita. Mientras tanto, me dedicaré a otros proyectos no tan grandes pero que se puedan terminar y que tengan algo de feedback. Como por ejemplo el juego Nurse Curse del que hablaré en otra entrada.
</p>
