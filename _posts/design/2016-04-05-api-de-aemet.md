---
layout: page
subheadline: Miscelaneo
title:  "API de AEMET"
teaser: "Una manera de poder leer los datos de AEMET de manera más o menos sencilla"
meta_teaser: "Una manera de comunicarse con los datos de AEMET"
breadcrumb: false
categories:
    - miscelaneo
tags:
    - aemet
    - xml
author: rdelblanco
---
Seguro que más de uno ha buscado alguna vez la API de AEMET (Agencia Estatal de Meteorología) sin éxito. Siendo esta agencia de carácter público debería tener una API al estilo openweathermap y similares a nivel de España pero si buscamos algo del estilo api.aemet.es no hallamos nada.

Entonces ¿tiene AEMET una API? Bueno, no exactamente pero nos vale si queremos recoger datos meteorológicos del país. Fijaos en la siguiente dirección <a href="http://www.aemet.es/xml/municipios/localidad_28013.xml">http://www.aemet.es/xml/municipios/localidad_28013.xml</a>. Aquí encontramos un xml con los datos de predicción del tiempo en Aranjuez de los próximos siete días. Para buscar los datos de un municipio en concreto, el sistema va de la siguiente manera: debemos escribir la url http://www.aemet.es/xml/municipios/localidad_X.xml siendo X un código numérico que consiste en el código de provincia. En Madrid es 28 pero si tienes dudas de qué código es el que corresponde a tu provincia, coincide con las dos primeras cifras del código postal de tu localidad. Las otras tres cifras (que indican el municipio en concreto) son un número correlativo acorde con el orden alfabético que le corresponde a ese municipio.

Es decir, que si yo enlazo a <a href="http://www.aemet.es/xml/municipios/localidad_28001.xml">http://www.aemet.es/xml/municipios/localidad_28001.xml</a> salen los datos de La Acebeda (Madrid) ¿Por qué La Acebeda corresponde al código 001? Porque es el primer municipio de Madrid si los ordenamos por orden alfabético (Acebeda, La).

Con esta información se puede hacer una base de datos que relacione código con municipio y así poder descargar el XML. Nosotros ya hicimos el trabajo sucio y creamos una base de datos que relaciona provincia y municipio con los códigos de AEMET. <a href="http://katodia.com/wp-content/uploads/2016/04/aemet.zip">Podéis descargarlo aquí</a>.
